#!/usr/bin/env bash

#set -exo pipefail

# echo "params passed:"
# echo "$#"

# for param in $*
#     do
#         echo $param
#     done

# for param in $*
#     do
#         if [ -d "$param" ]
#         then
#             echo "executing scripts in $param folder..."
#             ls -lh "$param"
#         else
#             echo "not in it..." 
#         fi
#     done

# FILES=$(ls test-dir/)
# NEW="new"

# for FILE in $FILES
#     do
#         echo "Rename $FILE to $NEW-$FILE" 
#     done

FILES=$(ls test-dir/)

for FILE in $FILES
    do
        if [ -f "$FILE" ] 
        then
            echo "$FILE is a File"
        else
            echo "$FILE is a Dir"
        fi
    done

# AZ=$(aws ec2 describe-availability-zones --region us-east-1 --output text --query AvailabilityZones[*].[ZoneName])

# for zone in $AZ
#     do
#         echo "zone is: $zone"
#     done