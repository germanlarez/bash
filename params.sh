#!/usr/bin/env bash

# set -exo pipefail

# Know all params passed to a script
echo "All params passed $*"
echo "number of params: $#"
# 

USER_GROUP=$1

if [ "$USER_GROUP" == "glarez" ]
then
    echo "Usergroup" $USER_GROUP "has permission to execute"
elif [ "$USER_GROUP" == "admin" ]
then
    echo "Of course Admin has permission to execute"
else
    echo "Do not have permissions!"
fi

USER_DIR=$2

if [ -d "$USER_DIR" ]
then
    echo "Exito! "$USER_DIR" Exists"
else
    echo "Creating Userdir and config file config.yaml inside"
    mkdir "$USER_DIR"
    touch "$USER_DIR/config.yaml"
fi