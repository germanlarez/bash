#!/usr/bin/env bash
# set -exo pipefail

echo "Server Setup..."

CONFIG_FILE=config.yaml
SERVER_NAME=$(hostname)

if [ -d "config" ]
then
    echo "Reading Config Dir..."
    CONFIG_FILES=$(ls config)
    echo "Config files found:"
    echo $CONFIG_FILES
else
    echo "Config dir not found, creating one and copying config files..."
    mkdir config && cp -r /home/glarez/GIT/06-1-jenkinsSharedLibrary/vars/* config/
    echo $CONFIG_FILES
fi

echo "Using file $CONFIG_FILE to setup server: $SERVER_NAME"